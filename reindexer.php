<?php
$ch = curl_init(); //Intit CURL

$url = 'https://reporter.seznam.cz/wm-api/web/document/reindex'; //URL Webmaster tools by Seznam.cz

$fields = array(
    "key" => "", //API KEY
    "url" => urlencode($_POST['urlToReindex']) //PAGE to reindex/index on Seznam.cz
);

$params = null; //Init param query

foreach($fields as $key=>$value) { $params .= $key.'='.$value.'&'; } //Foreach fields (params)
$params = rtrim($params, '&'); //(Remove last char)
$query = $url . "?" . $params; //Create URL for CURL

curl_setopt($ch, CURLOPT_URL, $query); //Set URL
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);

$result = curl_exec($ch); //Exec

//Print result of query
$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
switch ($httpcode) {
    case 200:
        echo "Přidáno do databáze k reindexaci";
        break;
    case 400:
        echo "v dotazu chybí API klíč (parametr key)";
        break;
    case 401:
        echo "nesprávný API klíč";
        break;
    case 403:
        echo "přístup odepřen";
        break;
    default:
        echo "služba je mimo provoz";
        break;
}
